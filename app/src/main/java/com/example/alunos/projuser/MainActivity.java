package com.example.alunos.projuser;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class MainActivity extends AppCompatActivity {

    ImageView user, adduser, place;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        user = (ImageView) findViewById(R.id.imageView3);
        adduser = (ImageView) findViewById(R.id.imageView4);
        place = (ImageView) findViewById(R.id.imageView5);


    user.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Intent i = new Intent(MainActivity.this,UserActivity.class);
            startActivity(i);
        }
    });
        adduser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(MainActivity.this,AddUserActivity.class);
                startActivity(i);
            }
        });

        place.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              Intent i = new Intent(Intent.ACTION_VIEW);
              i.setData(Uri.parse("geo:-6.3396586,-47.4021591?z=17"));
              Intent chooser = Intent.createChooser(i, "Escolha o aplicativo");
              startActivity(chooser);

                //-6.3396586,-47.4021591
            }

        });

    }
}
