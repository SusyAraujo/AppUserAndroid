package com.example.alunos.projuser;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

public class UserActivity extends AppCompatActivity {

    ListView ls;

    String[] nomes={"Marcelo","Susy","João","Maria","TelmaBala","Antonio","Lorrane","Vecanandre",
                    "Steffane","Smithe"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);

        ls = (ListView) findViewById(R.id.list_nomes);

        ArrayAdapter<String> adapter =  new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, nomes);
        ls.setAdapter(adapter);

        ls.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(getApplicationContext(), "Item selecionado foi o"+i,Toast.LENGTH_SHORT).show();
            }
        });
    }
}
